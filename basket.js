function Container() {
  this.id = '';
  this.className = '';
  this.htmlCode = '';
}

Container.prototype.render = function() {
  return this.htmlCode;
}

function Basket() {
  Container.call(this);
  
  this.id = 'basket';
  this.countGoods = 0;
  this.amount = 0;
  
  this.basket_items = [];
  
  this.retreive();
}

Basket.prototype = Object.create(Container.prototype);
Basket.prototype.constructor = Basket;

Basket.prototype.render = function() {
  var basketItems = $('<div/>', {
    id: this.id + '_items'
  });
  $('#' + this.id).append(basketItems);
}

Basket.prototype.retreive = function() {
  $.get({
    url: '/basket/get',
    context: this,
    success: function(response) {
      var basketData = $('<div/>', {
        id: 'basket_data'
      });
      this.countGoods = response.basket.length;
      this.basket_items = response.basket;
      this.amount = response.amount;
      basketData.text('Товаров в корзине на общую сумму - ' + this.amount + ' рублей');
      $('#' + this.id).append(basketData);
    },
    dataType: 'json'
  })
}

Basket.prototype.add = function(idProduct, price, quantity) {
  this.countGoods += quantity;
  this.amount += price * quantity;
  this.basket_items.push({
    id_product: idProduct,
    price: price,
    quanity: quanity
  });
  this.refresh();
}

Basket.prototype.refresh = function() {
  var basketData = $('#basket_data');
  basketData.empty();
  basketData.text('Товаров в корзине на общую сумму - ' + this.amount + ' рублей');
}
