/* global fetch:false */
/* global alert:false */
/* eslint-disable no-console */
// TODO: вроде как должно съесться, но ты проверь
import * as $ from 'jquery';

class Review {
  constructor(userId) {
    this.userId = userId;
    this.reviewURL = 'http://localhost:3000/review';
    this.responses = [];
  }

  addReview(text) {
    const fetchAdd = fetch(`${this.reviewURL}/add`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      // TODO: не надо кавычек ключам объекта
      body: JSON.stringify({
        idUser: this.userId,
        text,
      }),
    });
    fetchAdd
      .then(response => response.json())
      // eslint-disable-next-line no-alert
      .then(responseBody => ((responseBody.result === 1) ? alert(responseBody.userMessage) : console.log('fck')))
      .then(this.listReview())
      .catch();
  }

  submitReview(idCommentForLike) {
    const fetchSubmit = fetch(`${this.reviewURL}/submit`, {
      headers: {
        'Content-type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({
        idComment: idCommentForLike, // use camelCase for keys
      }),
    });
    fetchSubmit
      .then(response =>
        response.json()).then((responseBody) => {
        if (responseBody.result === 1) {
          this.listReview();
        }
      })
      .catch();
  }

  deleteReview(commentId) {
    const fetchReview = fetch(`${this.reviewURL}/delete`, {
      headers: {
        'Content-type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({
        idComment: commentId,
      }),
    });
    fetchReview.then(response => response.json())
      .then((responseBody) => {
        if (responseBody.result === 1) this.listReview();
      })
      .catch();
  }

  listReview() {
    this.responses.length = 0;
    const fetchList = fetch(`${this.reviewURL}/list`, {
      headers: {
        'Content-type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({}),
    });
    fetchList
      .then((response) => { console.log(response); return response.json(); })
      .then(responseBody => {console.log(responseBody); return responseBody.comments; })
      // TODO: es6 rules!
      // for (let comment of comments) {
      //   this.responsess.push(comment);
      // }
      .then(comments => comments.map(comment => this.responses.push(comment)))
      .then(() => this.render())
      .then(() => console.log('obnovil'))
      .catch();
  }

  render() {
    $('#review').text('')
      .append($('<div/>', {
        id: 'submit-block',
      }).append($('<input/>', {
        id: 'submit-text',
      })).append($('<button/>', {
        value: 'Like',
        text: 'submit',
        id: 'sub',
      })).on('click', 'button#sub', () => {
        // FIXME: надо бы избавиться от использования до объявления
        reviewer.addReview($('#submit-text').val());
      }));
    this.responses.forEach((item) => {
      const comment = $('<div/>', {
        // id: item.idComment,
        id: item.id,
      });
      comment
      // .append(`<div>${item.text}</div>`) TEMPORARY
        .append(`<div>${item.location}</div>`)
        .append('<button class="like">+</button><button class="del">X</button>')
        .on('click', 'button.del', () => {
          // this.deleteReview(item.idComment);
          this.deleteReview(item.id);
        })
        .on('click', 'button.like', () => {
          // this.submitReview(item.idComment);
          this.submitReview(item.id);
        });
      $('#review').append(comment);
    });
  }
}

const reviewer = new Review(1);
reviewer.listReview();
