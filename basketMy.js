/* eslint-disable no-unused-vars */
class Constainer {
  constructor() {
    this.id = '';
    this.className = '';
    this.htmlCode = '';
  }

  render() {
    return this.htmlCode;
  }
}

class Basket extends Constainer {
  constructor(userId) {
    super();
    this.user = userId;
    this.id = 'basket';
    this.amount = 0;
    this.fullPrice = 0;
    this.basketItems = [];
  }

  getBasket() {
    const getFetch = fetch('http://localhost:3000/basket/get', {
      headers: ({ 'Content-type': 'application/json' }),
      method: 'POST',
      body: JSON.stringify({ 'user_id': this.user })
    });
    getFetch.then((response) => {
      return response.json();
    }).then((responseBody) => {
      if (responseBody.result === 0 || typeof responseBody.result === 'undefined') {
        if (responseBody.result === 0) {
          throw new BasketError(`can't receive ths basket: ${responseBody.error_message}`);
        }
        throw new BasketError('can\'t receive the basket');
      }
      this.amount = responseBody.amount;
      const items = responseBody.basket;
      for (let item in items) {
        this.basketItems.push(items[ item ]);
      }
    }).then(() => this.render())
      .catch(/* i don't know how catch exception */);
  }

  add(product, quantity) {
    const fetchAddBasket = fetch('http://localhost:3000/basket/add', {
      headers: ({ 'Content-type': 'application/json' }),
      method: 'POST',
      body: JSON.stringify({
        'user_id': this.user,
        'product_id': product,
        'quantity': quantity
      }),
    });
    fetchAddBasket.then((response) => {
      return response.json()
    })
      .then((responseBody) => {
        if (responseBody.result === 0 || typeof responseBody.result === 'undefined') {
          if (responseBody.result === 0) {
            throw new BasketError(`can't add the good in the basket: ${responseBody.error_message}`);
          }
          throw new BasketError('can\'t add the good in the basket');
        }
        this.fullPrice = responseBody.full_price;
      })
      .then(this.render())
      .catch(/* i don't know how catch this exception. I will read about this */);
  }

  removeGood(product, price) {
    const removeFetch = fetch('http://localhost:3000/basket/delete', {
      headers: { 'Content-type': 'application/json' },
      method: 'POST',
      body: JSON.stringify({ 'id_product': product, 'full_price': price })
    });
    removeFetch.then(response => response.json())
      .then(responseBody => {
        if (responseBody.result === 0 || responseBody.result === 'undefined') {
          if (responseBody.result === 0) {
            throw new BasketError(`can't remove good: ${responseBody.error_message}`);
          }
          throw new BasketError(`can't remove good`);
        }
      })
      .then(this.render())
      .catch(/* haven't tt, result is hz */);
  }


  // todo: this function is unsupported at present time
  // todo: need create the render
  render() {
    super.render();
    console.log('i\'m render');
  }
}
